import 'package:flutter/material.dart';
class CustomTextField extends StatelessWidget {
  final String labelText;
  final String hintText;
  final TextEditingController? controller;
  final ValueChanged<String> onChanged;
  final bool hasError;

  const CustomTextField({super.key,
    required this.labelText,
    required this.hintText,
    this.controller,
    required this.onChanged,
    this.hasError = false,
  });

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.all(8.0),
      child: TextField(
        decoration: InputDecoration(
          labelText: hasError ? 'Lỗi: $labelText' : labelText,
          hintText: hintText,
          hintStyle: const TextStyle(
            fontSize: 14,
          ),
          border: const OutlineInputBorder(),
        ),
        controller: controller,
        onChanged: onChanged,
      ),
    );
  }
}





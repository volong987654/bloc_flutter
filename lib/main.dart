import 'package:api_bloc/screen/event_screen/event_screen.dart';
import 'package:api_bloc/screen/login_screen/login.dart';
import 'package:api_bloc/screen/login_screen/login_controller.dart';
import 'package:firebase_core/firebase_core.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_easyloading/flutter_easyloading.dart';
import 'core/app_bloc.dart';
import 'package:flutter/foundation.dart';

void main() async{
  WidgetsFlutterBinding.ensureInitialized();
  if(kIsWeb){
   // await Firebase.initializeApp(
   //      options: FirebaseOptions(
   //          apiKey: apiKey,
   //          appId: appId,
   //          messagingSenderId: messagingSenderId,
   //          projectId: projectId));
    print("chạy web");
  }  else {
    print("chạy app");
    await Firebase.initializeApp(
     // options: DefaultFirebaseOptions.currentPlatform,
    );
  }
  LoginController.getUserLoggedInStatus();
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({super.key});
  @override
  Widget build(BuildContext context) {
    return MultiBlocProvider(
      providers: AppBlocProviders.allBlocProviders,
      // providers: [...AppPages.allBlocProvider(context)],
      child: MaterialApp(
        debugShowCheckedModeBanner: false,
        theme: ThemeData(
            appBarTheme: const AppBarTheme(
          elevation: 0,
          backgroundColor: Colors.white,
        )),
        home: LoginController.isSignedIn ? const EventScreen() : const LoginScreen(),
        builder: EasyLoading.init(),
        // onGenerateRoute: AppPages.GenerateRouteSettings,
        // routes: {
        //   "loginPage": (context) => const Login(),
        //   "home": (context)=> const HomePage(),
        // },
      ),
    );
  }
}

import 'dart:convert';

User userFromJson(String str) => User.fromJson(json.decode(str));

String userToJson(User data) => json.encode(data.toJson());

class User {
  int? userId;
  int? id;
  String? title;
  String? body;
  String? error;

  User({
    this.userId,
    this.id,
    this.title,
    this.body,
    this.error,
  });

  factory User.fromJson(Map<String, dynamic> json) => User(
    userId: json["userId"],
    id: json["id"],
    title: json["title"],
    body: json["body"],
    error: json["error"],
  );

  Map<String, dynamic> toJson() => {
    "userId": userId,
    "id": id,
    "title": title,
    "body": body,
  };

  User.withError(String message){
    error = message;
  }

}
import 'package:flutter_bloc/flutter_bloc.dart';
import 'home_events.dart';
import 'home_state.dart';

class AppBlocs extends Bloc<AppEvent, AppState>{
  AppBlocs() : super(AppState()){
    on<TriggerAppEvent>((event, emit){
      emit(AppState(index: state.index));
    });
  }
}
import 'package:api_bloc/screen/messenger_screen/mess_screen.dart';
import 'package:api_bloc/screen/profile_screen/profile_screen.dart';
import 'package:flutter/material.dart';

import '../../event_screen/event_screen.dart';

Widget buildPage(int index){
  List<Widget> widget = [
    const EventScreen(),
    const MessScreen(),
    const ProfileScreen(),
  ];
  return widget[index];
}
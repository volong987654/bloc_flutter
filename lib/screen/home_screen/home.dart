import 'package:api_bloc/screen/home_screen/widget/application_page.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

import 'bloc/home_bloc.dart';
import 'bloc/home_events.dart';
import 'bloc/home_state.dart';

class HomeScreen extends StatefulWidget {
  const HomeScreen({super.key});

  @override
  State<HomeScreen> createState() => _HomeScreenState();
}


class _HomeScreenState extends State<HomeScreen> {
  @override
  Widget build(BuildContext context) {
    return  BlocBuilder<AppBlocs, AppState>(
        builder:(context, state) {
          return Container(
              color: Colors.white,
              child: SafeArea(
                child: Scaffold(
                  body: buildPage(state.index),
                  bottomNavigationBar: BottomNavigationBar(
                    onTap: (value) {
                      state.index = value;
                      BlocProvider.of<AppBlocs>(context).add(TriggerAppEvent(value));
                    },
                    elevation: 0,
                    items: const <BottomNavigationBarItem>[
                      BottomNavigationBarItem(
                        icon: Icon(Icons.home),
                        label: 'Home',
                      ),
                      BottomNavigationBarItem(
                        icon: Icon(Icons.business),
                        label: 'Messenger',
                      ),
                      BottomNavigationBarItem(
                        icon: Icon(Icons.school),
                        label: 'Profile',
                      ),
                    ],
                    currentIndex: state.index,
                    selectedItemColor: Colors.blue,
                    unselectedItemColor: Colors.black,
                    //onTap: _onItemTapped,
                  ),
                ),
              )
          );
        }
    );
  }
  }

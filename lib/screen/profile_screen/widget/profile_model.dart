import 'package:flutter/material.dart';

class Profile{
  final String? title;
  final IconData? icon;

  Profile({
    this.title,
    this.icon,
  });

}

final List<Profile> profile = [
  Profile(title: 'My Account', icon: Icons.star),
  Profile(title: 'Question', icon: Icons.favorite),
  Profile(title: 'LogOut', icon: Icons.bookmark),
];
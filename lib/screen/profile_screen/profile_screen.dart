import 'package:api_bloc/core/strings.dart';
import 'package:api_bloc/screen/profile_screen/widget/profile_model.dart';
import 'package:flutter/material.dart';

import '../login_screen/login.dart';

class ProfileScreen extends StatefulWidget {
  const ProfileScreen({super.key});

  @override
  State<ProfileScreen> createState() => _ProfileScreenState();
}

class _ProfileScreenState extends State<ProfileScreen> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: appbarShows(context, AppString.profile),
      body: ListView.builder(
        itemCount: profile.length,
        itemBuilder: (context, index) {
          return ListTile(
            leading: Icon(profile[index].icon),
            title: Text(profile[index].title.toString()),
            onTap: () {
              switch (index) {
                case 0:
                  print(index);
                case 1:
                  print(index);
                case 2:
                  print(index);
              } // Gọi hàm printInfo()
            },
          );
        },
      ),
    );
  }
}

PreferredSizeWidget appbarShows(context, String? text) {
  bool isNeedSafeArea = MediaQuery.of(context).viewPadding.bottom > 0;
  return PreferredSize(
    preferredSize: Size.fromHeight(
      MediaQuery.of(context).padding.top + kToolbarHeight,
    ),
    child: Container(
      decoration: appBarBackLinearGradient(),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.center,
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          SizedBox(
            height: isNeedSafeArea == true ? 35 : 15,
          ),
          Padding(
            padding: const EdgeInsets.symmetric(horizontal: 20),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.center,
              crossAxisAlignment: CrossAxisAlignment.center,
              children: [
                Text(
                  text ?? "Profile",
                  style: const TextStyle(
                    fontSize: 24,
                    fontWeight: FontWeight.bold,
                    color: Colors.white,
                  ),
                ),
              ],
            ),
          ),
        ],
      ),
    ),
  );
}

import 'package:flutter/material.dart';

class SplashScreen extends StatelessWidget {
  const SplashScreen({super.key});

  @override
  Widget build(BuildContext context) {
    return const Scaffold(
      body: Stack(
        children: [
          Image(
            image: AssetImage('assets/images/unsplash_bg.jpg'),
            fit: BoxFit.cover,
            height: double.infinity,
            width: double.infinity,
            alignment: Alignment.center,
          ),
          Center(
            child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              crossAxisAlignment: CrossAxisAlignment.center,
              children: [
                Text(
                  'Stryker Labs',
                  style: TextStyle(
                    fontSize: 54,
                    color: Colors.white,
                    letterSpacing: -2,
                  ),
                ),
                Text(
                  'PlayerApp',
                  style: TextStyle(
                    fontSize: 54,
                    color: Colors.white,
                    letterSpacing: -2,
                  ),
                ),
              ],
            ),

          ),
        ],
      ),
    );
  }
}

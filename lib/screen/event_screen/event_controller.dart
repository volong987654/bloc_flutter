import 'package:dio/dio.dart';

import '../../model/event.dart';

class ApiProvider{
  Dio dio = Dio();
  final String _url = "https://jsonplaceholder.typicode.com/posts";

  Future<List<User>> fetchUserList() async{
    print("gọi bloc truyền isloading ở đây");
    try {
      final response = await dio.get(_url);
      List<dynamic> value = response.data;
      return value.map((e) => User.fromJson(e)).toList();
    } catch(e){
       if (e.toString().contains("SocketException")){
         return [User.withError("Check your internet connection please")];
       }
       return [User.withError(e.toString())];
    }
  }
}
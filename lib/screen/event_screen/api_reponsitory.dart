import 'package:api_bloc/screen/event_screen/event_controller.dart';

import '../../model/event.dart';

class ApiRepository {
  final ApiProvider _apiProvider = ApiProvider();

  Future<List<User>> fetchUserList(){
    return _apiProvider.fetchUserList();
  }

}

class NetWorkError extends Error {

}
import 'package:equatable/equatable.dart';

import '../../../model/event.dart';

abstract class UserState extends Equatable{
  const UserState();

  @override
  List<Object> get props => [];
}

class UserInital extends UserState {}

class UserLoading extends UserState {}

class UserLoaded extends UserState {
  final List<User> userList;
  const UserLoaded({ required this.userList});}

class UserError extends UserState {
  final String? error;
  const UserError({this.error});}
import 'package:api_bloc/screen/event_screen/api_reponsitory.dart';
import 'package:api_bloc/screen/event_screen/bloc/ev_event.dart';
import 'package:api_bloc/screen/event_screen/bloc/event_state.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

import '../../../model/event.dart';

class UserBloc extends Bloc<UserEvent, UserState>{
 UserBloc() : super(UserInital()){
   final ApiRepository apiRepository = ApiRepository();
   on((event, emit) async {
    try{
      emit(UserLoading());
      final List<User> userList = await apiRepository.fetchUserList();
      emit(UserLoaded(userList: userList));
      if(userList[0].error != null){
        emit(UserError(error: userList[0].error));
      }
    } on NetWorkError {
       emit(const UserError(error: "Failed to fetch data is your device online"));
    }
   });
 }
}
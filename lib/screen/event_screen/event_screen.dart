import 'package:api_bloc/screen/event_screen/api_reponsitory.dart';
import 'package:api_bloc/screen/event_screen/bloc/ev_event.dart';
import 'package:api_bloc/screen/event_screen/bloc/event_bloc.dart';
import 'package:api_bloc/screen/event_screen/bloc/event_state.dart';
import 'package:api_bloc/screen/event_screen/event_controller.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_easyloading/flutter_easyloading.dart';
import '../../model/event.dart';
import '../profile_screen/profile_screen.dart';

class EventScreen extends StatefulWidget {
  const EventScreen({super.key});

  @override
  State<EventScreen> createState() => _EventScreenState();
}

class _EventScreenState extends State<EventScreen> {
  final UserBloc _userBloc = UserBloc();

  @override
  void initState(){
    _userBloc.add(GetUserList());
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: appbarShows(context, "Home"),
      body: _buildListUser(),
    );
  }

  Widget _buildListUser(){
    return Card(
      child: BlocProvider(create: (context) => _userBloc,
        child: RefreshIndicator(
          onRefresh: () {
            return ApiProvider().fetchUserList();
          },
          child: BlocBuilder<UserBloc, UserState>(
            builder: (context, state){
               if (state is UserError) {
                 return const Center(child: Text("Error App"));
               } if (state is UserInital) {
                   EasyLoading.show(
                   status: 'Please Wait!',
                   maskType: EasyLoadingMaskType.black,
                 );
               } if (state is UserLoading) {
                 EasyLoading.show(
                   status: 'Please Wait!',
                   maskType: EasyLoadingMaskType.black,
                 );
                // return const Center(child: CircularProgressIndicator());
               } if (state is UserLoaded) {
                 EasyLoading.dismiss(animation: true);
                 return ListView.builder(
                     itemCount: state.userList.length,
                     itemBuilder: (context, index){
                       User user = state.userList[index];
                       return ListTile(
                         title: Text(user.title.toString()),
                       );
                     });
               } else {
                   EasyLoading.dismiss(animation: true);
                   return const Text("Long");
               }
            },
          ),
        ),

      ),
    );
  }
}

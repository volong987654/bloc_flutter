class SignInState {
  final String email;
  final String password;
  final String dropButton;
  final bool isLoading;

  const SignInState({this.email = "", this.password = "", this.dropButton = "", this.isLoading = false});

  SignInState copyWith({String? email, String? password, String? dropButton, bool? isLoading}){
    return SignInState(
      email: email ?? this.email,
      password: password ?? this.password,
      dropButton: dropButton ?? this.dropButton,
      isLoading: isLoading ?? this.isLoading,
    );
  }
}

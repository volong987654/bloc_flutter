import 'package:api_bloc/screen/login_screen/bloc/sign_in_events.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import '../login.dart';
import 'login_state.dart';

class SignInBloc extends Bloc<SignInEvent, SignInState>{
  SignInBloc() : super(SignInState(
    dropButton: LoginScreen.list.first,
  )){
    on<EmailEvent>(_emailEvent);

    on<PasswordEvent>(_passwordEvent);

    on<DropMenuEvent>(_dropEvent);

    on<IsLoadingEvent>(_isLoading);
  }

  void _emailEvent(EmailEvent event, Emitter<SignInState> emit){
    emit(state.copyWith(email: event.email));
  }

  void _passwordEvent(PasswordEvent event, Emitter<SignInState> emit){
    emit(state.copyWith(password: event.password));
  }

  void _dropEvent(DropMenuEvent event, Emitter<SignInState> emit){
    emit(state.copyWith(dropButton: event.dropButton));
  }

  void _isLoading(IsLoadingEvent event, Emitter<SignInState> emit){
    emit(state.copyWith(isLoading: event.isLoading));
  }
}
import 'package:api_bloc/core/strings.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import '../../widget/custom_button.dart';
import '../../widget/text_field.dart';
import 'bloc/login_state.dart';
import 'bloc/sign_in_bloc.dart';
import 'bloc/sign_in_events.dart';

class LoginScreen extends StatefulWidget {
  static List<String> list = <String>['coach', 'player', 'subcoach'];
  const LoginScreen({Key? key}) : super(key: key);

  @override
  State<LoginScreen> createState() => _LoginScreenState();
}

class _LoginScreenState extends State<LoginScreen> {
  TextEditingController myController = TextEditingController();

  @override
  void dispose() {
    myController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return BlocBuilder<SignInBloc, SignInState>(builder: (context, state) {
      return Scaffold(
        appBar: appbarShow(context),
        body: SingleChildScrollView(
          child: Padding(
            padding: const EdgeInsets.all(8.0),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                const SizedBox(height: 100,),
                stringHeader(AppString.welcomeBack, 23, Colors.black, FontWeight.bold),
                stringHeader(AppString.signContinue, 14, Colors.black, FontWeight.normal),
                const SizedBox(height: 10,),
                CustomTextField(
                  labelText: 'UserName',
                  hintText: 'UserName',
                  //controller: myController,
                  onChanged: (value) {context.read<SignInBloc>().add(EmailEvent(value));
                  },
                  // hasError: isError, // isError là một biến xác định có lỗi hay không
                ),
                CustomTextField(
                  labelText: 'Password',
                  hintText: 'YourPassword',
                  // controller: myController,
                  onChanged: (value) {
                    context.read<SignInBloc>().add(PasswordEvent(value));
                  },
                  // hasError: isError, // isError là một biến xác định có lỗi hay không
                ),
                dropButton((value) {
                  context
                      .read<SignInBloc>()
                      .add(DropMenuEvent(value));
                }, state.dropButton),
                const SizedBox(
                  height: 10,
                ),
                CustomButton(
                  text:'Login',
                  onPressed: () {
                    context.read<SignInBloc>().add(const IsLoadingEvent(true));
                    //LoginController(context: context).login();
                  },
                  isLoading: state.isLoading,
                ),
              ],
            ),
          ),
        ),
      );
    });

  }
}

Widget stringHeader(String text, double? size, Color color, FontWeight fontWeight){
  return Text(text, style: TextStyle(
    fontSize: size ?? 23,
    color: color,
    fontWeight: fontWeight
  ),);
}

Widget dropButton(void Function(String value)? func, String text) {
  return Padding(
    padding: const EdgeInsets.all(8.0),
    child: SizedBox(
      height: 60,
      child: InputDecorator(
        decoration: const InputDecoration(
          border: OutlineInputBorder(
            borderSide: BorderSide(color: Colors.black, width: 1),
          ),
          focusedBorder: OutlineInputBorder(
            borderSide: BorderSide(color: Colors.black, width: 1),
          ),
          enabledBorder: OutlineInputBorder(
            borderSide: BorderSide(color: Colors.black, width: 1),
          ),
        ),
        child: DropdownButton<String>(
          value: text,
         // icon: const Icon(Icons.arrow_downward),
          elevation: 16,
          style: const TextStyle(color: Colors.deepPurple),
          onChanged: (String? value) => func!(value ?? LoginScreen.list.first),
          items: LoginScreen.list.map<DropdownMenuItem<String>>((String value) {
            return DropdownMenuItem<String>(
              value: value,
              child: Text(value),
            );
          }).toList(),
        ),
      ),
    ),
  );
}

PreferredSizeWidget appbarShow(context) {
  bool isNeedSafeArea = MediaQuery.of(context).viewPadding.bottom > 0;
  return PreferredSize(
    preferredSize: const Size.fromHeight(62),
    child: Container(
      decoration: appBarBackLinearGradient(),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.center,
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          SizedBox(
            height: isNeedSafeArea == true ? 30 : 15,
          ),
          const Padding(
            padding: EdgeInsets.symmetric(horizontal: 20),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.center,
              crossAxisAlignment: CrossAxisAlignment.center,
              children: [
                Text(
                  AppString.header,
                  style: TextStyle(
                    fontSize: 24,
                    fontWeight: FontWeight.bold,
                    color: Colors.white,
                  ),
                ),
              ],
            ),
          ),
        ],
      ),
    ),
  );
}

BoxDecoration appBarBackLinearGradient() {
  return const BoxDecoration(
    borderRadius: BorderRadius.only(
      bottomLeft: Radius.circular(16.0),
      bottomRight: Radius.circular(16.0),
    ),
    gradient: LinearGradient(
      begin: Alignment.topLeft,
      end: Alignment.bottomRight,
      colors: [
        Color(0xff0F2247),
        Color(0xff0084FF),
      ],
    ),
  );
}

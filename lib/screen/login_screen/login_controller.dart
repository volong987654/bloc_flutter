import "package:api_bloc/helper/helper_function.dart";
import "package:flutter/material.dart";
import "package:flutter_bloc/flutter_bloc.dart";

import "../../api/api_manager.dart";
import "../../widget/popup.dart";
import "../home_screen/home.dart";
import "bloc/sign_in_bloc.dart";
import 'package:http/http.dart' as http;

import "bloc/sign_in_events.dart";

class LoginController {
  final BuildContext context;
  static bool isSignedIn = false;
  const LoginController({required this.context});

  static getUserLoggedInStatus() async {
    // await HelperFunctions.getUserLoginStatus().then((value) => {
    //   if(value != null){
    //     isSignedIn = value,
    //   }
    // });
  }

  // void login() {
  //   final state = context.read<SignInBloc>().state;
  //   String emailAddress = state.email;
  //   String password = state.password;
  //   String coach = state.dropButton;
  //   bool isLogin = false;
  //
  //   if (emailAddress.isEmpty) {
  //     showAlertMessage(context, 'Error', '1!');
  //     context.read<SignInBloc>().add(const IsLoadingEvent(false));
  //   } else if (password.isEmpty) {
  //     context.read<SignInBloc>().add(const IsLoadingEvent(false));
  //     showAlertMessage(context, 'Error', '2!');
  //   }
  //
  //   if (emailAddress.isNotEmpty && password.isNotEmpty) {
  //     isLogin = true;
  //
  //   }
  //
  //   if (isLogin) {
  //     loginApiCall(emailAddress, password, coach);
  //   }
  // }

  // void loginApiCall(String username, String password, String type) async {
  //   http.StreamedResponse res =
  //       await ApiManager().loginApi(username, password, type);
  //
  //   if (res.statusCode == 200) {
  //     Navigator.of(context).pushAndRemoveUntil(
  //       MaterialPageRoute(
  //         builder: (context) => const HomeScreen(),
  //       ),
  //           (route) => false,
  //     );
  //   } else {
  //     showAlertMessage(context, 'Error', 'email or password!');
  //   }
  // }
}

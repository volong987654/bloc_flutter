import 'package:http/http.dart' as http;
import 'api.dart';

class ApiManager {
  Future<http.StreamedResponse> loginApi(String username, String pass, String type) async {
    var headers = {'Content-Type': 'application/x-www-form-urlencoded'};
    var request = http.Request('POST', Uri.parse(Api.loginAPI));
    request.bodyFields = {'username': username, 'password': pass, 'type': type};
    request.headers.addAll(headers);
    http.StreamedResponse response = await request.send();
    return response;
  }
}
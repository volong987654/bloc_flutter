class AppString{
  AppString._();
  //Login
  static const password = "Password";
  static const yourPassword = 'Your password*';
  static const userName = 'Username*';
  static const yourName = 'JohnSmith';
  static const coach = 'coach';
  static const player = 'player';
  static const subcoach = 'subcoach';
  static const welcomeBack = 'Welcome back';
  static const signContinue = 'Sign to continue';
  static const summit = 'Submit';
  static const header = 'Strykerlabs PlayerApp';
  static const messengers = 'Messengers';
  static const selectContact = "Select Contact";
  static const newGroup = "New Group";
  static const addParticipants = "Add participants";
  static const profile = "Profile";
  //token
  static const token = "Token";
  static const position = "Position";
}
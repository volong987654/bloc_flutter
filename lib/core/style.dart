import 'dart:ui';
import 'colors.dart';

class StyleConstants {
  StyleConstants._();
  static TextStyle onBackgroundText = TextStyle(
      fontSize: 20,
      color: ColorConstants.onLightFade,
      fontWeight: FontWeight.w700);

  static TextStyle primaryButtonText = TextStyle(
    //textStyle: const TextStyle(color: Palette.on_primary,),
    fontWeight: FontWeight.w600,
    fontSize: 22,
    color: ColorConstants.onPrimary,
  );
  static TextStyle lightButtonText = TextStyle(
    fontSize: 18,
    color: ColorConstants.onLight,
    fontFamily: 'Poppins',
    fontWeight: FontWeight.w600,
  );
  static TextStyle secondaryButtonText = TextStyle(
    fontSize: 18,
    color: ColorConstants.onSecondary,
    fontFamily: 'Poppins',
    fontWeight: FontWeight.w600,
  );
  static TextStyle errorButtonText = TextStyle(
    fontSize: 18,
    color: ColorConstants.onError,
    fontFamily: 'Poppins',
    fontWeight: FontWeight.w600,
  );
}

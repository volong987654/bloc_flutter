import 'package:api_bloc/screen/event_screen/bloc/event_bloc.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

import '../screen/home_screen/bloc/home_bloc.dart';
import '../screen/login_screen/bloc/sign_in_bloc.dart';

class AppBlocProviders {
  static get allBlocProviders => [
    BlocProvider(
      create: (context) => SignInBloc(),
    ),
    BlocProvider(
      create: (context) => AppBlocs(),
    ),
    // BlocProvider(
    //   create: (context) => UserBloc(),
    // ),
    // BlocProvider(
    //   create: (context) => UiEventBloc(),
    // ),
  ];
}
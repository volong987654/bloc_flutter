import 'package:flutter/material.dart';

class ColorConstants {
  const ColorConstants._();
  static const themeColor = Color(0xfff5a623);
  static const primaryColor = Color(0xff203152);
  static const greyColor = Color(0xffaeaeae);
  static const greyColor2 = Color(0xffE8E8E8);
  static const gradientColor1 = Color(0xef02469d);
  static const indigo = Colors.indigo;
  static const gradientColor2 = Color(0xff0F2247);
  static const gradientColor3 = Color(0xff0084FF);
  static const onPrimary =  Color(0xFFFFFFFF);
  static const onSecondary =  Color(0xFFFFFFFF);
  static const onError =  Color(0xFFFFFFFF);
  static const onLight = Color(0xFF585858);
  static const blackColor = Color(0xFF000000);
  static const onLightFade =  Color(0xFF777777);
}
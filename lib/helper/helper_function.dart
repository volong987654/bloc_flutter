import 'package:shared_preferences/shared_preferences.dart';

class HelperFunctions{
  //keys
   static String userLoggedKey = "LOGGEDINKEY";
   static String userNameKey = "USERNAMEKEY";
   static String userEmailKey = "USEREMAILKEY";

   static Future<bool?> getUserLoginStatus(bool isUserLoggedIn) async{
     SharedPreferences sf = await SharedPreferences.getInstance();
     return sf.setBool(userLoggedKey, isUserLoggedIn);
   }
   static Future<bool?> getUserNameSF(String userName) async{
     SharedPreferences sf = await SharedPreferences.getInstance();
     return sf.setString(userNameKey, userName);
   }

   static Future<bool?> getUserEmailSF() async{
     SharedPreferences sf = await SharedPreferences.getInstance();
     return sf.setString(userEmailKey, userEmailKey);
   }
}